import "react-native-gesture-handler";

import React from "react";
import { StatusBar } from "expo-status-bar";
import AppLoading from "expo-app-loading";
import {
  Inter_400Regular,
  Inter_500Medium,
  Inter_700Bold,
} from "@expo-google-fonts/inter";

import { useFonts } from "expo-font";

import { Routes } from "./src/routes";
import { View } from "react-native";

// redux
import { Provider } from "react-redux";
import { store } from "./src/redux/state/store";

export default function App() {
  const [fontsLoaded] = useFonts({
    Inter_400Regular,
    Inter_500Medium,
    Inter_700Bold,
    "Roboto-Black": require("./assets/fonts/Roboto-Black.ttf"),
    "Roboto-Bold": require("./assets/fonts/Roboto-Bold.ttf"),
    "Roboto-Italic": require("./assets/fonts/Roboto-Italic.ttf"),
    "Roboto-MediumItalic": require("./assets/fonts/Roboto-MediumItalic.ttf"),
    "Rowdies-Bold": require("./assets/fonts/Rowdies-Bold.ttf"),
    "Rowdies-Light": require("./assets/fonts/Rowdies-Light.ttf"),
    "Rowdies-Regular": require("./assets/fonts/Rowdies-Regular.ttf"),
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <Provider store={store}>
      <View
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}
      >
        <StatusBar style="light" backgroundColor="transparent" translucent />

        <Routes />
      </View>
    </Provider>
  );
}
