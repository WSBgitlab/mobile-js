module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    plugins: [
      "inline-dotenv",
      "@babel/plugin-proposal-export-namespace-from",
      [
        "module:react-native-dotenv",
        {
          envName: "UP4TECH_ENV",
          moduleName: "@env",
          path: ".env",
        },
      ],
    ],
  };
};
