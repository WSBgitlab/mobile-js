import React from "react";
import { View, Text, ScrollView } from "react-native";
import { Card, Title, Paragraph, Button } from "react-native-paper";
import { styles } from "./styles";
import { Ionicons } from "@expo/vector-icons";

import { StartSimulation } from "../StartSimulation/index";

export function Analysys() {
  return (
    <ScrollView style={{ backgroundColor: "#fff" }}>
      <StartSimulation></StartSimulation>
      <View style={styles.listactivities}>
        <View style={styles.contentListActivities}>
          <Text style={styles.titleListActivities}>Últimas simulações </Text>

          <View>
            <Ionicons name="notifications-circle" size={28} color="#fff" />
          </View>
        </View>
        <View>
          <Card style={styles.card}>
            <Card.Content style={styles.card_content}>
              <Title style={styles.cardTitle}>Casa Pinheiros</Title>

              <View style={styles.viewCardParagraph}>
                <Paragraph style={styles.paragraph}>R$: 600.000,00</Paragraph>
              </View>
              <View style={styles.btnContainer}>
                <View style={styles.btnStylization}>
                  <Text style={styles.btnText}>Visualizar</Text>
                </View>
              </View>
            </Card.Content>
          </Card>

          <Card style={styles.card}>
            <Card.Content style={styles.card_content}>
              <Title style={styles.cardTitle}>Apartamento Vila Olimpia</Title>

              <View style={styles.viewCardParagraph}>
                <Paragraph style={styles.paragraph}>R$: 4.835.580,14</Paragraph>
              </View>
              <View style={styles.btnContainer}>
                <View style={styles.btnStylization}>
                  <Text style={styles.btnText}>Visualizar</Text>
                </View>
              </View>
            </Card.Content>
          </Card>

          <Card style={styles.card}>
            <Card.Content style={styles.card_content}>
              <Title style={styles.cardTitle}>Apartamento Campinas</Title>

              <View style={styles.viewCardParagraph}>
                <Paragraph style={styles.paragraph}>R$: 1.658.540,19</Paragraph>
              </View>
              <View style={styles.btnContainer}>
                <View style={styles.btnStylization}>
                  <Text style={styles.btnText}>Visualizar</Text>
                </View>
              </View>
            </Card.Content>
          </Card>
        </View>
      </View>
    </ScrollView>
  );
}
