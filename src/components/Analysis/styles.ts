import { StyleSheet } from "react-native";
import { getBottomSpace } from "react-native-iphone-x-helper";
import { theme } from "../../styles/theme";

export const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.background,
  },
  activities: {
    width: "100%",
    paddingHorizontal: 30,
    paddingVertical: 50,
    backgroundColor: "#fff",
  },
  listactivities: {
    width: "100%",
    paddingHorizontal: 30,
    paddingVertical: 50,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: "#1f2937",
  },
  contentListActivities: {
    paddingVertical: 20,
    marginBottom: 15,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  titleListActivities: {
    fontSize: 22,
    color: "#fff",
  },
  card: {
    marginBottom: 30,
    borderBottomWidth: 1,
    borderBottomColor: "#0284c7",
  },
  card_content: {
    backgroundColor: "#1f2937",
  },
  cardTitle: {
    color: "#fff",
    fontSize: 20,
    textShadowColor: "#0284c7",
    textShadowRadius: 10,
    paddingBottom: 10,
  },
  viewCardParagraph: {
    padding: 1,
    borderWidth: 2,
    borderRadius: 4,
    width: "60%",
    borderColor: "#0284c7",
  },
  paragraph: {
    color: "#f1f1f1",
    fontWeight: "900",
  },
  btnContainer: {
    paddingVertical: 10,
  },
  btnStylization: {
    width: "40%",
    padding: 5,
    backgroundColor: "#0284c7",
    borderBottomEndRadius: 2,
    borderTopLeftRadius: 4,
  },
  btnText: {
    fontSize: 16,
    textAlign: "center",
    color: "#f8fafc",
  },
});
