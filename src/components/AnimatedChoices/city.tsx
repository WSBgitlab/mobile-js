import React, { useEffect, useState } from "react";
import LottieView from "lottie-react-native";

import { styles } from "./styles";

import { View } from "react-native";

const Properties = require("../../assets/city-building.json");

export function AnimatedCity() {
  const animation = React.createRef();

  useEffect(() => {
    animation.current.play();
  }, []);

  return (
    <View style={styles.contentAnimation}>
      <LottieView loop={true} ref={animation} source={Properties} />
    </View>
  );
}
