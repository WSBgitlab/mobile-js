import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  contentAnimation: {
    maxWidth: 250,
    borderBottomWidth: 2,
    borderColor: "#e2e8f0",
    flex: 1,
    alignItems: "center",
    width: "100%",
    height: 350,
    backgroundColor: "#ffffff",
  },
});
