import React, { useEffect, useState } from "react";
import { Text, View } from "react-native-animatable";
import { Picker, SafeAreaView, TextInput } from "react-native";
import { actionCreators } from "../../../redux/state/index";
import { useDispatch } from "react-redux";
import { styles } from "../styles";
import { bindActionCreators } from "@reduxjs/toolkit";

import Axios from "axios";

interface Params {
  modelSelected: string;
}

export function ModelsCars({ modelSelected }: Params) {
  const [model, setModel] = useState("");
  const dispatch = useDispatch();
  const { setSimulationVehicle } = bindActionCreators(actionCreators, dispatch);
  const [data, setData] = useState([]);

  // Function for request
  async function loadBrandsCode() {
    const response = await Axios.get(
      `https://parallelum.com.br/fipe/api/v1/carros/marcas/${modelSelected}/modelos`
    );

    console.log(
      `https://parallelum.com.br/fipe/api/v1/carros/marcas/${modelSelected}/modelos`
    );
    setData(response.data.modelos);
  }

  // Hooks
  useEffect(() => {
    setSimulationVehicle({
      models: model,
    });
  }, [model]);

  useEffect(() => {
    // Using an IIFE
    (async function requestBrands() {
      await loadBrandsCode();
    })();
  }, [modelSelected]);

  return (
    <View>
      <SafeAreaView>
        <Text style={styles.title_card}>Modelos</Text>
        <Picker
          style={styles.picker}
          selectedValue={model}
          onValueChange={(itemValue, itemIndex) => setModel(itemValue)}
        >
          {data.map((data: { nome: string; codigo: string }) => (
            <Picker.Item
              label={data.nome}
              value={data.codigo}
              key={data.codigo}
            />
          ))}
        </Picker>
      </SafeAreaView>
    </View>
  );
}
