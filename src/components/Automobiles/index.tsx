import React, { useState, useCallback } from "react";
import CurrencyInput from "react-native-currency-input";
import { View, Text } from "react-native-animatable";
import { Image } from "react-native";

import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";
import { useNavigation, useRoute } from "@react-navigation/native";
import { Picker, SafeAreaView, TextInput, Alert } from "react-native";
import { useSelector } from "react-redux";

import { State } from "../../redux/state/index";

import { Cars } from "./cars/cars";
import { MotorCycles } from "./motorcycles/motorcycles";
import { Trucks } from "./trucks/trucks";

import { styles } from "./styles";

type Params = {
  typeInvest: string;
};

export function Automobiles() {
  const navigation = useNavigation();
  const [value, setValue] = useState(0);
  const [category, setCategory] = useState("b");
  const [typePayments, settypePayments] = useState("consorcio");
  const [year, setYear] = useState("2015");
  const [deadline, setDeadline] = useState("48");

  const route = useRoute();
  const { typeInvest } = route.params as Params;

  console.log(typeInvest, "type");

  const vehicle = useSelector((state: State) => state.formVehicle);

  async function handleBackToSimulation() {
    navigation.navigate("Simulation");
  }

  function validateYears(year: number) {
    const date = new Date();

    if (year < 2005 || year > date.getFullYear() + 1) {
      return true;
    }
  }

  function validateDeadLine(months: number) {
    if (months < 1 || months > 180) {
      return true;
    }
  }

  async function handleSubmitForm() {
    if (value == 0)
      Alert.alert(
        "Ops! Valor zerado",
        "Não será possível realizar a análise com dados incompletos, por favor realize o preenchimento e tente novamente."
      );

    if (value < 0 || value < 10000)
      Alert.alert(
        "Ops! Valor mínimo não atingido;",
        "Tente novamente com valores maiores que 10 mil reais."
      );

    console.log({
      value,
      category,
      typePayments,
      vehicle: vehicle.brand || "chevrolet",
      year,
      deadline,
    });
  }

  return (
    <ScrollView style={styles.scroll_view}>
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={handleBackToSimulation}>
            <Ionicons name="arrow-back" size={20} color="#1f2937" />
          </TouchableOpacity>
          <Text>Simulação - Automóveis</Text>
        </View>
        <View style={styles.contentForm}>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              height: 400,
              width: "100%",
            }}
          >
            <Image
              style={{
                width: 300,
                height: 400,
                position: "absolute",
                top: -20,
                left: 100,
              }}
              source={require("./../../assets/car.png")}
            />
            <View
              style={{
                position: "relative",
                top: 100,
              }}
            >
              <Text
                style={{
                  fontSize: 30,
                  marginBottom: 20,
                  color: "#1f2937",
                  fontFamily: "Roboto-Bold",
                }}
              >
                Automóveis
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: "Roboto-Bold",
                  color: "#c2c2c2",
                }}
              >
                Seu novo investimento em auto.
              </Text>
              <Text
                style={{
                  fontSize: 13,
                  fontFamily: "Roboto-Bold",
                  color: "#c2c2c2",
                }}
              >
                Disponível para Caminhões, Carros e Motos.
              </Text>
            </View>
          </View>
        </View>
      </View>

      <View
        style={{
          borderTopLeftRadius: 25,
          borderTopRightRadius: 25,
          backgroundColor: "#1f2937",
        }}
      >
        <View style={{ paddingTop: 20, paddingHorizontal: 10 }}>
          <View
            style={{
              height: 100,
              justifyContent: "center",
              borderBottomWidth: 1,
              marginTop: 20,
              paddingHorizontal: 10,
              borderColor: "#007FF3",
            }}
          >
            <Text style={styles.title_card}>Valor</Text>

            {value < 0 && (
              <Text style={{ color: "#facc15", textAlign: "right" }}>
                Ops! valor inválido{" "}
              </Text>
            )}

            <CurrencyInput
              value={value}
              onChangeValue={setValue}
              prefix="R$"
              delimiter=","
              separator="."
              style={{ fontSize: 22, color: "#f1f1f1" }}
              precision={2}
              onChangeText={(formattedValue) => {
                console.log(formattedValue); // $2,310.46
              }}
            />
          </View>

          <View
            style={{
              height: 100,
              justifyContent: "center",
              borderBottomWidth: 1,
              paddingHorizontal: 10,
              borderColor: "#007FF3",
            }}
          >
            <Text style={{ color: "#f1f1f1", fontSize: 18 }}>
              Escolha categoria do automóvel
            </Text>
            <Picker
              style={styles.picker}
              selectedValue={category}
              onValueChange={(itemValue, itemIndex) => setCategory(itemValue)}
            >
              <Picker.Item label="Carro" value="b" />
              <Picker.Item label="Moto" value="a" />
              <Picker.Item label="Caminhão" value="c" />
            </Picker>
          </View>

          <View
            style={{
              height: 100,
              justifyContent: "center",
              borderBottomWidth: 1,
              marginTop: 20,
              paddingHorizontal: 10,
              borderColor: "#007FF3",
            }}
          >
            <Text style={{ color: "#f1f1f1", fontSize: 18 }}>
              Modelos de Pagamentos
            </Text>
            <Picker
              style={styles.picker}
              selectedValue={typePayments}
              onValueChange={(itemValue, itemIndex) =>
                settypePayments(itemValue)
              }
            >
              <Picker.Item label="Consórcio" value="consorcio" />
              <Picker.Item label="Financiamento" value="financiamento" />
              <Picker.Item label="Leasing" value="leasing" />
              <Picker.Item label="CDC" value="cdc" />
            </Picker>
          </View>

          <View style={{ paddingVertical: 3 }}>
            {category == "a" && <MotorCycles />}
            {category == "b" && <Cars />}
            {category == "c" && <Trucks />}
          </View>

          <View
            style={{
              height: 100,
              justifyContent: "center",
              borderBottomWidth: 1,
              marginTop: 20,
              paddingHorizontal: 10,
              borderColor: "#007FF3",
            }}
          >
            <Text style={styles.title_card}>Ano de fabricação</Text>

            {validateYears(parseInt(year)) && (
              <Text style={{ color: "#facc15", textAlign: "right" }}>
                Ops! valor inválido{" "}
              </Text>
            )}

            <SafeAreaView>
              <TextInput
                style={{ color: "#ccc" }}
                onChangeText={setYear}
                value={year}
                placeholder="2015"
                placeholderTextColor="#ccc"
                keyboardType="numeric"
              />
            </SafeAreaView>
          </View>

          <View
            style={{
              height: 100,
              justifyContent: "center",
              borderBottomWidth: 1,
              marginTop: 20,
              paddingHorizontal: 10,
              borderColor: "#007FF3",
            }}
          >
            <Text style={styles.title_card}>Prazo do pagamento</Text>

            {validateDeadLine(parseInt(deadline)) && (
              <Text style={{ color: "#facc15", textAlign: "right" }}>
                Ops! valor inválido{" "}
              </Text>
            )}

            <SafeAreaView>
              <TextInput
                style={{ color: "#ccc" }}
                onChangeText={setDeadline}
                value={deadline}
                placeholder="48x"
                placeholderTextColor="#ccc"
                keyboardType="numeric"
              />
            </SafeAreaView>
          </View>

          <View
            style={{
              justifyContent: "center",
              display: "flex",
              alignItems: "center",
              marginTop: 20,
              paddingBottom: 29,
            }}
          >
            <TouchableOpacity
              style={styles.btnStylization}
              onPress={handleSubmitForm}
            >
              <View style={styles.btnStylization}>
                <Text style={styles.btnText}>Analisar</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}
