import React, { useState, useEffect } from "react";
import { View, Text } from "react-native-animatable";
import { Picker, SafeAreaView, TextInput } from "react-native";
import { bindActionCreators } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import { actionCreators } from "../../../redux/state/index";

import { styles } from "./../styles";
import { ModelsMotorCycles } from "./models";

import Axios from "axios";

export function MotorCycles() {
  const [motorcycles, setMotorCycles] = useState("");
  const [brands, setBrands] = useState([]);
  const dispatch = useDispatch();
  const { setSimulationVehicle } = bindActionCreators(actionCreators, dispatch);

  useEffect(() => {
    setSimulationVehicle({
      brand: motorcycles,
    });
  }, [motorcycles]);

  async function loadContent() {
    const response = await Axios.get(
      "https://parallelum.com.br/fipe/api/v1/motos/marcas"
    );

    setBrands(response.data);
  }

  useEffect(() => {
    // Using an IIFE
    (async function requestBrands() {
      await loadContent();
    })();
  }, []);

  return (
    <View
      style={{
        height: 200,
        justifyContent: "space-evenly",
        borderBottomWidth: 1,
        marginTop: 20,
        paddingHorizontal: 10,
        borderColor: "#007FF3",
      }}
    >
      <SafeAreaView>
        <Text style={styles.title_card}>Marca</Text>
        <Picker
          style={styles.picker}
          selectedValue={motorcycles}
          onValueChange={(itemValue, itemIndex) => setMotorCycles(itemValue)}
        >
          {brands.map((data: { nome: string; codigo: string }) => (
            <Picker.Item
              label={data.nome}
              value={data.codigo}
              key={data.codigo}
            />
          ))}
        </Picker>
      </SafeAreaView>
      <ModelsMotorCycles modelSelected={motorcycles} />
    </View>
  );
}
