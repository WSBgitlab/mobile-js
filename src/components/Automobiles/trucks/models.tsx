import React, { useEffect, useState } from "react";
import { Text, View } from "react-native-animatable";
import { Picker, SafeAreaView, TextInput } from "react-native";

import { useDispatch } from "react-redux";
import { bindActionCreators } from "@reduxjs/toolkit";
import { actionCreators } from "../../../redux/state/index";

import { styles } from "../styles";

import Axios from "axios";

interface Params {
  modelSelected: string;
}

interface Attributes {
  code: string;
  name: string;
}

export function ModelsTrucks({ modelSelected }: Params) {
  const [model, setModel] = useState("");
  const [data, setData] = useState<Attributes[]>([]);

  const dispatch = useDispatch();
  const { setSimulationVehicle } = bindActionCreators(actionCreators, dispatch);

  // Function for request
  async function loadBrandsCode() {
    const response = await Axios.get(
      `https://parallelum.com.br/fipe/api/v2/trucks/brands/${modelSelected}/models`
    );

    setData(response.data);
  }

  useEffect(() => {
    // Using an IIFE
    (async function requestBrands() {
      await loadBrandsCode();
    })();
  }, [modelSelected]);

  return (
    <View>
      <SafeAreaView>
        <Text style={styles.title_card}>Modelos</Text>
        <Picker
          style={styles.picker}
          selectedValue={model}
          onValueChange={(itemValue, itemIndex) => setModel(itemValue)}
        >
          {data.map((data) => (
            <Picker.Item label={data.name} value={data.code} key={data.name} />
          ))}
        </Picker>
      </SafeAreaView>
    </View>
  );
}
