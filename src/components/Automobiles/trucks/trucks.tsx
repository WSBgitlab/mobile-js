import React, { useState, useEffect } from "react";
import { View, Text } from "react-native-animatable";
import { Picker, SafeAreaView, TextInput } from "react-native";
import { bindActionCreators } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import { actionCreators } from "../../../redux/state/index";

import { styles } from "./../styles";
import { ModelsTrucks } from "./models";

import Axios from "axios";

interface Attributes {
  name: string;
  code: string;
}

export function Trucks() {
  const [trucks, setTrucks] = useState("");
  const [brands, setBrands] = useState([]);
  const dispatch = useDispatch();
  const { setSimulationVehicle } = bindActionCreators(actionCreators, dispatch);

  useEffect(() => {
    setSimulationVehicle({
      brand: trucks,
    });
  }, [trucks]);

  async function loadContent() {
    const response = await Axios.get(
      "https://parallelum.com.br/fipe/api/v2/trucks/brands"
    );

    console.log(response.data, "brands");
    setBrands(response.data);
  }

  useEffect(() => {
    // Using an IIFE
    (async function requestBrands() {
      await loadContent();
    })();
  }, []);

  return (
    <View
      style={{
        height: 200,
        justifyContent: "space-evenly",
        borderBottomWidth: 1,
        marginTop: 20,
        paddingHorizontal: 10,
        borderColor: "#007FF3",
      }}
    >
      <SafeAreaView>
        <Text style={styles.title_card}>Marca</Text>
        <Picker
          style={styles.picker}
          selectedValue={trucks}
          onValueChange={(itemValue, itemIndex) => setTrucks(itemValue)}
        >
          {brands.map((data: { name: string; code: string }) => (
            <Picker.Item label={data.name} value={data.code} key={data.code} />
          ))}
        </Picker>
      </SafeAreaView>
      <ModelsTrucks modelSelected={trucks} />
    </View>
  );
}
