import React from "react";
import * as Animatable from "react-native-animatable";
import { Text, View, TouchableOpacity } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { FontAwesome5, Entypo } from "@expo/vector-icons";
import * as AuthSession from "expo-auth-session";

import { styles } from "./styles";
import { useNavigation } from "@react-navigation/native";
import { theme } from "../../styles/theme";

type AuthResponse = {
  type: string;
  params: {
    access_token: string;
  };
  name: string;
};

export function FooterLogin() {
  const navigation = useNavigation();

  const CLIENT_ID =
    "11498690570-4olega66mmlfshgquk4gjmo0u210vuig.apps.googleusercontent.com";
  const REDIRECT_URI = "https://auth.expo.io/@wsb-engineer/up4tech";

  async function handleSignIn() {
    const RESPONSE_TYPE = "token";
    const SCOPE = encodeURI("profile email");

    const authUrl = `https://accounts.google.com/o/oauth2/v2/auth?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=${RESPONSE_TYPE}&scope=${SCOPE}`;

    const { type, params } = (await AuthSession.startAsync({
      authUrl,
    })) as AuthResponse;

    if (type == "success") {
      navigation.navigate("Main", { token: params.access_token });
    }
  }

  return (
    <Animatable.View
      animation="fadeInUpBig"
      style={[
        styles.footer,
        {
          backgroundColor: "#4b5563",
          height: "98%",
        },
      ]}
    >
      <View>
        <Text style={styles.text_footer}>Login</Text>
      </View>
      <Text style={{ color: "#fff" }}>Acesse com sua conta do google</Text>
      <View style={styles.button}>
        <TouchableOpacity style={styles.signIn} onPress={handleSignIn}>
          <LinearGradient colors={["#f9fafb", "#f9fafb"]} style={styles.signIn}>
            <Entypo
              name="google--with-circle"
              size={24}
              color={theme.colors.blueDark}
            />
            <Text
              style={[
                styles.textSign,
                {
                  color: "#0f172a",
                },
              ]}
            >
              Entar com o Google
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
      <View
        style={{
          alignItems: "center",
          height: "15%",
          marginTop: 10,
          justifyContent: "flex-end",
        }}
      >
        <Text style={{ color: "#ccc" }}>Termos e politicas</Text>
      </View>
    </Animatable.View>
  );
}
