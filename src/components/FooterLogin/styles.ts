import { StyleSheet } from "react-native";
import { theme } from "../../styles/theme";

export const styles = StyleSheet.create({
  footer: {
    backgroundColor: "#94a3b8",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_footer: {
    fontSize: 21,
    marginBottom: 8,
    color: "#fff",
    fontFamily: "Roboto",
  },
  button: {
    alignItems: "center",
    marginTop: 30,
  },
  signIn: {
    width: "80%",
    height: 30,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 14,
    paddingLeft: 5,
  },
});
