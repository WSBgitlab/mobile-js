import React from "react";
import { View, Text, Image } from "react-native";

import { styles } from "./styles";
import { Ionicons } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation, useRoute } from "@react-navigation/native";

type Profile = {
  profile: string;
};

type Params = {
  token: string;
};

export function MenuHeader() {
  const navigation = useNavigation();
  const route = useRoute();

  const { token } = route.params as Params;

  async function handleProfile() {
    navigation.navigate("Profile", { token });
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.actionMenu}>
          <TouchableOpacity onPress={handleProfile}>
            <Ionicons
              style={styles.imageHeader}
              name="ios-menu"
              size={18}
              color="#18181b"
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}
