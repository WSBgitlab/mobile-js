import { StyleSheet } from "react-native";
import { getStatusBarHeight } from "react-native-iphone-x-helper";
import { theme } from "../../styles/theme";

export const styles = StyleSheet.create({
  container: {
    width: "100%",
    display: "flex",
    paddingTop: getStatusBarHeight() + 10,
    backgroundColor: "#fffc",
    borderBottomWidth: 5,
    borderColor: "#cbd5e1",
  },
  header: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  actionMenu: {
    justifyContent: "space-between",
    flexDirection: "row",
    width: "100%",
  },
  imageHeader: {
    paddingHorizontal: 5,
    paddingVertical: 6,
  },
  welcomeUser: {
    color: "#18181b",
    fontSize: 18,
    fontWeight: "900",
    fontStyle: "normal",
  },
});
