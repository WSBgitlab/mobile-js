import React, { useState, useCallback, useEffect } from "react";
import CurrencyInput from "react-native-currency-input";
import { View, Text } from "react-native-animatable";
import { Image } from "react-native";
import Axios from "axios";

import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";
import { useNavigation, useRoute } from "@react-navigation/native";
import { Picker, SafeAreaView, TextInput, Alert } from "react-native";
import { useSelector } from "react-redux";

import { State } from "../../redux/state/index";
import { styles } from "./styles";

type Params = {
  typeInvest: string;
};

interface Locality {
  id: string;
  nome: string;
  sigla: string;
}

export function Properties() {
  const navigation = useNavigation();
  // value
  const [value, setValue] = useState(0);
  // types invest
  const [category, setCategory] = useState("apto");
  const [typePayments, settypePayments] = useState("consorcio");
  const [deadline, setDeadline] = useState("48");
  const [model, setModel] = useState("");

  // states, citys and County.
  const [states, setStates] = useState<Locality[]>([]);
  const [city, setCity] = useState<Locality[]>([]);

  // Selected places
  const [formStates, setformStates] = useState("AC");
  const [formCity, setFormCity] = useState("");
  const [formDistrict, setDistrict] = React.useState("");

  async function loadStates() {
    const responseStates = await Axios.get(
      "https://servicodados.ibge.gov.br/api/v1/localidades/estados?orderBy=nome"
    );

    const responseCity = await Axios.get(
      `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${formStates}/municipios`
    );

    setStates(responseStates.data);
    setCity(responseCity.data);
    setFormCity(responseCity.data[0].nome);
  }

  useEffect(() => {
    (async function requestBrands() {
      await loadStates();
    })();
  }, [formStates, formCity]);

  async function handleBackToSimulation() {
    navigation.navigate("Simulation");
  }

  async function handleSubmitForm() {
    if (value == 0)
      return Alert.alert(
        "Ops! Valor zerado",
        "Não será possível realizar a análise com dados incompletos, por favor realize o preenchimento e tente novamente."
      );

    if (value < 0 || value < 10000)
      return Alert.alert(
        "Ops! Valor mínimo não atingido;",
        "Tente novamente com valores maiores que 10 mil reais."
      );

    if ((formCity || formStates) == "") {
      return Alert.alert(
        "Ops! Valores incorretos",
        "Estado ou Cidade sem preencher.."
      );
    }

    if (formDistrict == "") {
      return Alert.alert(
        "Ops! Valores incorretos",
        "Munícipio ou Bairro sem preencher."
      );
    }

    console.log({
      value,
      category,
      model,
      formStates,
      formCity,
      formDistrict,
      deadline,
    });
  }

  return (
    <ScrollView style={styles.scroll_view}>
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={handleBackToSimulation}>
            <Ionicons name="arrow-back" size={20} color="#1f2937" />
          </TouchableOpacity>
          <Text>Simulação - Imóveis</Text>
        </View>
        <View style={styles.contentForm}>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              height: 400,
              width: "100%",
            }}
          >
            <Image
              style={{
                width: 300,
                height: 300,
                position: "absolute",
                top: 0,
                left: 120,
              }}
              source={require("./../../assets/house2.png")}
            />
            <View
              style={{
                position: "relative",
                top: 100,
              }}
            >
              <Text
                style={{
                  fontSize: 30,
                  marginBottom: 20,
                  color: "#1f2937",
                }}
              >
                Imóveis
              </Text>
              <Text
                style={{
                  fontSize: 18,
                  color: "#b2b2b2",
                }}
              >
                Meu novo imóvel
              </Text>
              <Text
                style={{
                  fontSize: 13,
                  color: "#b2b2b2",
                }}
              >
                Escolha seu melhor investimento em imóveis.
              </Text>
            </View>
          </View>
        </View>
      </View>

      <View
        style={{
          borderTopLeftRadius: 25,
          borderTopRightRadius: 25,
          backgroundColor: "#1f2937",
        }}
      >
        <View style={{ paddingTop: 20, paddingHorizontal: 10 }}>
          <View
            style={{
              height: 100,
              justifyContent: "center",
              borderBottomWidth: 1,
              marginTop: 20,
              paddingHorizontal: 10,
              borderColor: "#007FF3",
            }}
          >
            <Text style={styles.title_card}>Valor</Text>

            {value < 0 && (
              <Text style={{ color: "#facc15", textAlign: "right" }}>
                Ops! valor inválido{" "}
              </Text>
            )}

            <CurrencyInput
              value={value}
              onChangeValue={setValue}
              prefix="R$"
              delimiter=","
              separator="."
              style={{ fontSize: 22, color: "#f1f1f1" }}
              precision={2}
              onChangeText={(formattedValue) => {
                console.log(formattedValue); // $2,310.46
              }}
            />
          </View>

          <View
            style={{
              height: 100,
              justifyContent: "center",
              borderBottomWidth: 1,
              paddingHorizontal: 10,
              borderColor: "#007FF3",
            }}
          >
            <Text style={styles.title_card}>Escolha categoria do Imóvel</Text>
            <Picker
              style={styles.picker}
              selectedValue={category}
              onValueChange={(itemValue, itemIndex) => setCategory(itemValue)}
            >
              <Picker.Item label="Apartamento" value="apto" />
              <Picker.Item label="Casa" value="casa" />
              <Picker.Item label="Galpão" value="galpao" />
              <Picker.Item label="Terreno" value="terreno" />
            </Picker>
          </View>

          {(category == "apto" || category == "casa") && (
            <View
              style={{
                height: 100,
                justifyContent: "center",
                borderBottomWidth: 1,
                paddingHorizontal: 10,
                borderColor: "#007FF3",
              }}
            >
              <Text style={styles.title_card}>Modelo de investimento</Text>
              <Picker
                style={styles.picker}
                selectedValue={category}
                onValueChange={(itemValue, itemIndex) => setModel(category)}
              >
                <Picker.Item label="Construção" value="construcao" />
                <Picker.Item label="Novo" value="novo" />
                <Picker.Item label="Usado" value="usado" />
              </Picker>
            </View>
          )}
          {category == "terreno" && (
            <View
              style={{
                height: 100,
                justifyContent: "center",
                borderBottomWidth: 1,
                paddingHorizontal: 10,
                borderColor: "#007FF3",
              }}
            >
              <Text style={styles.title_card}>Modelo de investimento</Text>
              <Picker
                style={styles.picker}
                selectedValue={category}
                onValueChange={(itemValue, itemIndex) => setModel(category)}
              >
                <Picker.Item label="Residencial" value="residencial" />
                <Picker.Item label="Empresárial" value="empresarial" />
              </Picker>
            </View>
          )}
          {category == "galpao" && (
            <View
              style={{
                height: 100,
                justifyContent: "center",
                borderBottomWidth: 1,
                paddingHorizontal: 10,
                borderColor: "#007FF3",
              }}
            >
              <Text style={styles.title_card}>Modelo de investimento</Text>
              <Picker
                style={styles.picker}
                selectedValue={category}
                onValueChange={(itemValue, itemIndex) => setModel(category)}
              >
                <Picker.Item label="Consórcio" value="residencial" />
                <Picker.Item
                  label="Financiamento Bancário"
                  value="empresarial"
                />
              </Picker>
            </View>
          )}

          <View
            style={{
              height: 100,
              justifyContent: "center",
              borderBottomWidth: 1,
              paddingHorizontal: 10,
              borderColor: "#007FF3",
            }}
          >
            <Text style={styles.title_card}>Estado do Imóvel</Text>
            <Picker
              style={styles.picker}
              selectedValue={typePayments}
              onValueChange={(itemValue, itemIndex) => setformStates(itemValue)}
            >
              {states.map((data) => (
                <Picker.Item
                  label={data.nome}
                  value={data.sigla}
                  key={data.id}
                />
              ))}
            </Picker>
          </View>

          <View
            style={{
              height: 100,
              justifyContent: "center",
              borderBottomWidth: 1,
              paddingHorizontal: 10,
              borderColor: "#007FF3",
            }}
          >
            <Text style={styles.title_card}>Cidade</Text>
            <Picker
              style={styles.picker}
              selectedValue={typePayments}
              onValueChange={(itemValue, itemIndex) => setFormCity(itemValue)}
            >
              {city.map((data) => (
                <Picker.Item label={data.nome} value={data.id} key={data.id} />
              ))}
            </Picker>
          </View>

          <View
            style={{
              height: 100,
              justifyContent: "center",
              borderBottomWidth: 1,
              paddingHorizontal: 10,
              borderColor: "#007FF3",
            }}
          >
            <Text style={styles.title_card}>Distrito</Text>
            <TextInput
              style={{ color: "#ccc" }}
              onChangeText={setDistrict}
              placeholder="Informe bairro ou munícipio"
              placeholderTextColor="#fff"
              value={formDistrict}
            />
          </View>

          <View
            style={{
              height: 100,
              justifyContent: "center",
              borderBottomWidth: 1,
              paddingHorizontal: 10,
              borderColor: "#007FF3",
            }}
          >
            <Text style={styles.title_card}>Prazo do pagamento</Text>

            <SafeAreaView>
              <TextInput
                style={{ color: "#ccc" }}
                onChangeText={setDeadline}
                value={deadline}
                placeholder="48x"
                placeholderTextColor="#ccc"
                keyboardType="numeric"
              />
            </SafeAreaView>
          </View>
          <View
            style={{
              justifyContent: "center",
              display: "flex",
              alignItems: "center",
              marginTop: 20,
              paddingBottom: 29,
            }}
          >
            <TouchableOpacity
              style={styles.btnStylization}
              onPress={handleSubmitForm}
            >
              <View style={styles.btnStylization}>
                <Text style={styles.btnText}>Analisar</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}
