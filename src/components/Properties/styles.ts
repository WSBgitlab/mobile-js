import { StyleSheet } from "react-native";
import {
  getBottomSpace,
  getStatusBarHeight,
} from "react-native-iphone-x-helper";

export const styles = StyleSheet.create({
  scroll_view: {
    backgroundColor: "#fff",
    height: "100%",
  },
  container: {
    backgroundColor: "#f1f1f1",
    height: 500,
  },
  header: {
    backgroundColor: "#fff",
    width: "100%",
    paddingHorizontal: 10,
    paddingTop: getStatusBarHeight() + 10,
    paddingBottom: 10,
    borderBottomWidth: 5,
    borderColor: "#cbd5e1",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  close_action: {
    backgroundColor: "#fff",
  },
  contentAnimation: {
    borderColor: "#e2e8f0",
    width: "100%",
    flex: 3,
    backgroundColor: "#fff",
  },
  contentForm: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    height: 500,
    backgroundColor: "#fff",
  },
  card_styled: {
    borderRadius: 3,
    marginTop: 20,
    justifyContent: "space-between",
    borderWidth: 2,
    paddingHorizontal: 10,
    borderColor: "#1f2937",
    backgroundColor: "#1f2937",
    // #007FF3
  },
  title_card: {
    fontSize: 16,
    padding: 2,
    marginVertical: 2,
    color: "#fafafa",
    textAlign: "left",
  },
  drop_down_style: {
    width: "100%",
    borderWidth: 1,
    margin: 10,
    borderColor: "#007FF3",
    padding: 5,
  },
  picker: {
    height: 20,
    width: "50%",
    marginTop: 10,
    color: "#fff",
  },
  btnContainer: {
    paddingVertical: 10,
  },
  btnStylization: {
    width: "50%",
    padding: 5,
    backgroundColor: "#0284c7",
    borderBottomEndRadius: 2,
    borderTopLeftRadius: 4,
  },
  btnText: {
    fontSize: 13,
    textAlign: "center",
    color: "#f8fafc",
  },
});
