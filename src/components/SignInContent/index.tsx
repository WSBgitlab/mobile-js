import React, { useEffect } from "react";
import { View, Text, StatusBar } from "react-native";

import { styles } from "./styles";
import { Image } from "react-native";

import { FooterLogin } from "../../components/FooterLogin";

export function SignInContent() {
  return (
    <>
      <StatusBar backgroundColor="#4b5563" barStyle="light-content" />
      <View style={styles.image}>
        <View style={{}}>
          <Image
            style={{
              width: 80,
              height: 80,
            }}
            source={require("./../../../assets/logo_in.png")}
          />
        </View>
      </View>
      <View style={styles.header}>
        <Text style={styles.text_header}>Bem vindo(a)!</Text>
        <Text
          style={{
            width: "95%",
            backgroundColor: "#f4f4f5",
            height: 3,
            borderRadius: 30,
            marginBottom: 10,
          }}
        ></Text>
        <Text>Acesse e confira melhores condições de investimentos.</Text>
      </View>
      <FooterLogin />
    </>
  );
}
