import { StyleSheet } from "react-native";
import { theme } from "../../styles/theme";

export const styles = StyleSheet.create({
  image: {
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#f4f4f5",
    backgroundColor: "#fff",
  },
  header: {
    justifyContent: "center",
    backgroundColor: "#ffff",
    width: "100%",
    display: "flex",
    paddingHorizontal: 20,
    paddingVertical: 80,
    marginBottom: 10,
  },
  text_header: {
    color: "#000",
    fontWeight: "900",
    textShadowColor: "#fff",
    fontSize: 18,
    textShadowRadius: 10,
    textShadowOffset: { width: -1, height: 1 },
    paddingBottom: 1,
  },
  subInfo: {
    fontSize: 14,
    textShadowColor: "#fff",
    color: "#60a5fa",
    textShadowRadius: 3,
    textShadowOffset: { width: -1, height: 1 },
    paddingBottom: 1,
  },
});
