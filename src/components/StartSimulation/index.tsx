import React, { useEffect } from "react";
import { View, Image } from "react-native";
import { Card, Title, Paragraph } from "react-native-paper";

import { styles } from "./styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";
import { State } from "../../redux/state/index";

export function StartSimulation() {
  const navigation = useNavigation();
  const user = useSelector((state: State) => state.profile);

  async function handleLogout() {
    navigation.navigate("Simulation");
  }

  return (
    <View style={styles.activities}>
      <View style={styles.title_action}>
        <Card.Content>
          <Image
            style={{
              width: 50,
            }}
            source={require("./../../../assets/logo_m.png")}
          />
          <View>
            <Title>Olá, {user.name}</Title>
          </View>
        </Card.Content>
      </View>

      <Card style={styles.card}>
        <Card.Content>
          <Paragraph>Temos condiçoes para ajudar você.</Paragraph>
          <TouchableOpacity onPress={handleLogout}>
            <Title style={styles.card_title_init}>Iniciar</Title>
          </TouchableOpacity>
        </Card.Content>
      </Card>
    </View>
  );
}
