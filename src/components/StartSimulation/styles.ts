import { StyleSheet } from "react-native";
import { theme } from "../../styles/theme";

export const styles = StyleSheet.create({
  activities: {
    width: "100%",
    paddingHorizontal: 30,
    paddingVertical: 50,
    height: 400,
    backgroundColor: "#fff",
    justifyContent: "space-around",
  },
  title_action: {
    paddingVertical: 0,
    marginBottom: 0,
  },
  text_action: {
    fontSize: 22,
  },
  card: {
    marginBottom: 30,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 3.27,

    elevation: 3,
  },
  card_title: { fontSize: 18 },
  card_title_init: {
    fontSize: 14,
    width: "20%",
    padding: 1,
    textAlign: "center",
    borderWidth: 1,
    borderRadius: 4,
    borderColor: "#0284c7",
    backgroundColor: "#0284c7",
    color: "#f8fafc",
  },
  content_init: { alignItems: "center", paddingVertical: 10 },
  button_style: {
    borderWidth: 1,
    borderColor: theme.colors.blueDark,
    width: "30%",
    backgroundColor: "#fff",
  },
});
