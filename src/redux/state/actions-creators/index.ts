import { ActionType, ActionFormV } from "../actions-type";
import { Dispatch } from "@reduxjs/toolkit";
import { Action, FormV } from "../actions/index";

export const setProfile = (data: Object) => {
  return (dispatch: Dispatch<Action>) => {
    dispatch({
      type: ActionType.SETPROFILE,
      payload: data,
    });
  };
};

export const setSimulationVehicle = (data: Object) => {
  return (dispatch: Dispatch<FormV>) => {
    dispatch({
      type: ActionFormV.SETDATA,
      payload: data,
    });
  };
};
