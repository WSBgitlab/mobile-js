import { ActionType, ActionFormV } from "../actions-type/index";

interface setProfileAction {
  type: ActionType.SETPROFILE;
  payload: Object;
}

interface setFormVehicle {
  type: ActionFormV.SETDATA;
  payload: Object;
}

export type Action = setProfileAction;
export type FormV = setFormVehicle;
