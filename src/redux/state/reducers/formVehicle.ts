import { FormV } from "../actions/index";
import { ActionFormV } from "../actions-type/index";

const initialState = {
  value: "string",
  category: "chevrolet",
  typePayments: "string",
  brand: "string",
  year: "string",
  deadline: "string",
  model: "string",
};

type FormVehicle = {
  value: string;
  category: string;
  typePayments: string;
  brand: string;
  year: string;
  deadline: string;
};

const reducer = (state: FormVehicle = initialState, action: FormV) => {
  switch (action.type) {
    case ActionFormV.SETDATA:
      return (state = action.payload as FormVehicle);
    default:
      return state;
  }
};

export default reducer;
