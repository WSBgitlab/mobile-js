import { combineReducers } from "@reduxjs/toolkit";
import profileReducers from "./profileReducer";
import formVehicle from "./formVehicle";

const reducers = combineReducers({
  profile: profileReducers,
  formVehicle: formVehicle,
});

export default reducers;

export type State = ReturnType<typeof reducers>;
