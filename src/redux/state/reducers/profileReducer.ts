import { Action } from "../actions/index";
import { ActionType } from "../actions-type/index";

const initialState = {
  name: "string",
  email: "string",
  family_name: "string",
  given_name: "string",
  locale: "string",
  picture: "string",
};

type Profile = {
  name: string;
  email: string;
  family_name: string;
  given_name: string;
  locale: string;
  picture: string;
};

const reducer = (state: Profile = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.SETPROFILE:
      return (state = action.payload as Profile);
    default:
      return state;
  }
};

export default reducer;
