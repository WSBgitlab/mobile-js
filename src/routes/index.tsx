import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

import { Main } from "../screens/Main";
import { SignIn } from "../screens/SignIn";
import { Simulations } from "../screens/Simulation";
import { Profile } from "../screens/Profile";
import { FormType } from "../screens/FormType";

import { theme } from "../styles/theme";

const { Navigator, Screen } = createStackNavigator();

export function Routes() {
  return (
    <NavigationContainer>
      <Navigator headerMode="none">
        <Screen name="SignIn" component={SignIn} />
        <Screen name="Main" component={Main} />
        <Screen name="Simulation" component={Simulations} />
        <Screen name="Profile" component={Profile} />
        <Screen name="FormType" component={FormType} />
        <Screen name="StepTwoProperties" component={Profile} />
      </Navigator>
    </NavigationContainer>
  );
}
