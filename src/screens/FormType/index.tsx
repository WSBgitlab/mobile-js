import React, { useState, useCallback, useEffect } from "react";

import { View, Text } from "react-native-animatable";
import { useRoute } from "@react-navigation/native";
import { Automobiles } from "../../components/Automobiles";
import { Properties } from "../../components/Properties";

type Params = {
  typeInvest: string;
};

export function FormType() {
  const route = useRoute();
  const { typeInvest } = route.params as Params;

  return (
    <View>
      {typeInvest == "properties" && <Properties />}
      {typeInvest == "vehicles" && <Automobiles />}
    </View>
  );
}
