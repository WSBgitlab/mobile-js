import React, { useEffect, useState } from "react";
import { StatusBar } from "react-native";
import { useNavigation, useRoute } from "@react-navigation/native";

import { Analysys } from "../../components/Analysis";
import { theme } from "../../styles/theme";
import { MenuHeader } from "../../components/MenuHeader";

import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "@reduxjs/toolkit";
import { actionCreators, State } from "../../redux/state/index";

type Params = {
  token: string;
};

type Profile = {
  name: string;
  email: string;
  family_name: string;
  given_name: string;
  locale: string;
  picture: string;
};

export function Main() {
  const route = useRoute();
  const { token } = route.params as Params;

  const dispatch = useDispatch();

  const { setProfile } = bindActionCreators(actionCreators, dispatch);

  async function loadProfile() {
    const response = await fetch(
      `https://www.googleapis.com/oauth2/v2/userinfo?alt=json&access_token=${token}`
    );

    const userInfo = await response.json();

    setProfile({
      email: userInfo.email,
      family_name: userInfo.family_name,
      given_name: userInfo.given_name,
      locale: userInfo.locale,
      name: userInfo.name,
      picture: userInfo.picture,
    });
  }

  useEffect(() => {
    loadProfile();
  }, []);

  return (
    <>
      <StatusBar backgroundColor="#007FF3" barStyle="light-content" />
      <MenuHeader></MenuHeader>
      <Analysys />
    </>
  );
}
