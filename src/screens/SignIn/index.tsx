import React from "react";
import { View } from "react-native";

import { SignInContent } from "../../components/SignInContent";

import { styles } from "./styles";

export function SignIn() {
  return (
    <View style={styles.container}>
      <SignInContent />
    </View>
  );
}
