import React, { useEffect, useState } from "react";
import { View, Text } from "react-native-animatable";
import { ScrollView } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Card, Title, Paragraph } from "react-native-paper";

import { Picker } from "react-native";

import { styles } from "./styles";

import { AnimatedCity } from "../../components/AnimatedChoices/city";
import { AnimatedVehicle } from "../../components/AnimatedChoices/vehicle";
import { AnimatedInvest } from "../../components/AnimatedChoices/invest";
import { useNavigation } from "@react-navigation/native";

export function Simulations() {
  const [typeInvest, setTypeInvest] = useState("properties");
  const [animate, setAnimate] = useState(<AnimatedInvest />);
  const navigation = useNavigation();

  async function handleBackToMain() {
    navigation.navigate("Main");
  }

  async function handleToForm() {
    navigation.navigate("FormType", { typeInvest });
  }

  useEffect(() => {
    if (typeInvest == "investments") {
      setAnimate(<AnimatedInvest />);
    } else if (typeInvest == "vehicles") {
      setAnimate(<AnimatedVehicle />);
    } else {
      setAnimate(<AnimatedCity />);
    }
  }, [typeInvest]);

  return (
    <ScrollView style={styles.scroll_view}>
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={handleBackToMain}>
            <Ionicons name="arrow-back" size={20} color="#f1f1f1" />
          </TouchableOpacity>
          <Text>Iniciando Simulação</Text>
        </View>
        <View style={styles.contentForm}>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignContent: "center",
              alignItems: "center",
            }}
          >
            {animate}
          </View>
        </View>
      </View>
      <View
        style={{
          borderTopLeftRadius: 25,
          borderTopRightRadius: 25,
          backgroundColor: "#fff",
        }}
      >
        <View style={{ padding: 10, backgroundColor: "#1f2937" }}>
          <View style={styles.card_styled}>
            <Text style={styles.title_card}>Selecione seu investimento!</Text>
            <View style={styles.viewCardParagraph}>
              <Picker
                selectedValue={typeInvest}
                style={styles.picker}
                onValueChange={(itemValue, itemIndex) =>
                  setTypeInvest(itemValue)
                }
              >
                <Picker.Item label="Imóveis" value="properties" />
                <Picker.Item label="Veículos" value="vehicles" />
              </Picker>
            </View>

            <View>
              <Card style={styles.card}>
                <Card.Content
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <TouchableOpacity onPress={handleToForm}>
                    <Title style={styles.card_title_init}>Iniciar</Title>
                  </TouchableOpacity>
                </Card.Content>
              </Card>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}
