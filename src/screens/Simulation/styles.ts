import { StyleSheet } from "react-native";
import { getStatusBarHeight } from "react-native-iphone-x-helper";

export const styles = StyleSheet.create({
  card: {
    backgroundColor: "#1f2937",
    marginTop: 10,
    marginBottom: 30,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.34,
    shadowRadius: 3.27,
    justifyContent: "center",
    elevation: 3,
  },
  card_title: { fontSize: 18 },
  card_title_init: {
    fontSize: 12,
    paddingHorizontal: 3,
    textAlign: "center",
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#0284c7",
    backgroundColor: "#0284c7",
    color: "#f8fafc",
  },
  scroll_view: {
    height: "100%",
    backgroundColor: "#1f2937",
  },
  container: {
    backgroundColor: "#18277C",
  },
  viewCardParagraph: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 4,
    borderColor: "#f4f4f4",
    width: "100%",
    marginTop: 10,
  },
  paragraph: {
    color: "#f1f1f1",
    fontWeight: "900",
  },
  header: {
    backgroundColor: "#fff",
    width: "100%",
    paddingHorizontal: 10,
    paddingTop: getStatusBarHeight() + 10,
    paddingBottom: 10,
    borderBottomWidth: 5,
    borderColor: "#cbd5e1",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  close_action: {
    backgroundColor: "#f1f1",
  },
  contentAnimation: {
    borderBottomWidth: 2,
    borderColor: "#e2e8f0",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    backgroundColor: "#ffffff",
  },
  contentForm: {
    paddingHorizontal: 0,
    paddingVertical: 5,
    backgroundColor: "#fff",
  },
  card_styled: {
    borderRadius: 3,
    marginTop: 20,
    justifyContent: "space-between",
    paddingHorizontal: 10,
    // #007FF3
  },
  title_card: {
    fontSize: 20,
    color: "#fff",
    textAlign: "center",
  },
  drop_down_style: {
    width: "100%",
    borderWidth: 1,
    margin: 10,
    borderColor: "#007FF3",
    padding: 5,
  },
  picker: {
    height: 20,
    width: "100%",
    padding: 2,
    color: "#fff",
    fontWeight: "bold",
  },
  btnContainer: {
    paddingVertical: 10,
  },
  btnStylization: {
    width: "60%",
    height: 30,
    padding: 3,
    backgroundColor: "#f4f4f4",
    borderBottomEndRadius: 2,
    borderTopLeftRadius: 4,
  },
  btnText: {
    fontSize: 14,
    textAlign: "center",
    color: "#007FF3",
  },
});
